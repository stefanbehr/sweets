\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry}

\usepackage{graphicx}

\title{\textit{sweets}: A Speech-enabled Twitter Application}

\author{Stefan Behr\\
LING 575\\
Final Project}

\date{\today}

\begin{document}

\maketitle

\begin{abstract}
	Over part of this quarter, I have created a speech-based system which allows a user to interact with the popular social networking platform Twitter. In its current state, the system is a system-initiative speech application which, when called by a user, allows that user to post to a single, fixed Twitter user account, and to also listen to an arbitrary number of tweets from that account's tweet timeline. The system consists of a VoiceXML frontend, which is accompanied by JavaScript code and a few custom GSL grammars, as well as a Python backend component built, for the most part, on the Python Flask microframework. The backend component communicates with Twitter's public API via a Python library. The system's current implementation is less fully-featured than was originally envisioned. Some of the difficulties which contributed to this fact, as well as general implementation difficulties, are discussed. Proposed improvements to the system are discussed as well.
\end{abstract}

\section{Approach}

\subsection{Architecture}

The system, dubbed \textit{sweets} (a blend of \textit{speech} and \textit{tweets}), consists of both frontend and backend components.\\

The bulk of the frontend of the system is VoiceXML markup. This markup controls the dialogue management of the system and also occasionally communicates with the backend component, using JavaScript code to process HTTP responses received back from the backend component. The VXML also relies on two custom GSL grammars --- one for switching between tweeting and listening to tweets, and one for capturing utterances to be posted as tweets. The initiative model used in the system is system-initiative. This is partly due to the fact that, for any activity which the system allows the user to engage in (i.e., tweeting or listening to tweets), the number of different pieces of information required from the user is very limited. Additionally, there is little overlap between the different pieces of information required for different activities, which made a system-initiative implementation simply easier to complete.\\

The system's backend consists primarily of Python code. The code implements a simple web app which is able to respond to POST requests sent from the VXML frontend. Depending on the web app's URL route that the request is sent to, different Python functions trigger. One function communicates with the Twitter API to post a tweet to the timeline of the current authenticated user, after which no important data is returned to the frontend. Another route's function communicates with Twitter to pull up a requested number of tweets from the timeline of the current authenticated user, which are rendered into an XML template and sent back to the VXML frontend for processing via JavaScript, and subsequent presentation to the user via TTS.\\

A system architecture diagram is shown below:\\

\begin{figure}[!htbp]
\centering
\caption{System Architecture Diagram}
\label{nlim}
\includegraphics[scale=0.4]{arch.png}
\end{figure}

\subsection{Software and Data}

\subsubsection{Twitter}

The system's source of data for dynamic content is Twitter. The average experience on Twitter involves users, timelines, and tweets. A user logs in to Twitter and can then interact with timelines and tweets. A timeline consists of a chronological sequence of tweets. A tweet is an object whose main focus is a 140-character string of text, but which is also oriented around a user (the poster), date and time information, and geographic information (where the tweet was made). A user can both add tweets to his/her own timeline or read tweets from any timeline, as long as it is either public or has given the user read-rights (this topic is slightly more involved than it is necessary to discuss for the purposes of this system).\\

Programmatic interaction with Twitter data can be achieved through the Twitter API and language-specific wrappers. A popular Python library was used in this case. Use of the API requires authentication via OAuth to license both the application and user.

\subsubsection{Flask and Jinja2}

The system's backend uses Flask to implement a web app which can receive and respond to HTTP POST requests asking for tweets to be returned or dictating tweets to submit to Twitter. Though Flask is a microframework, it provides ample functionality for making the creation of web apps easy. This includes simple mapping of URLs to functions, which simplifies the dispatching of Twitter API requests.\\

Flask uses Jinja2 as its template rendering engine. This allows for easy rendering of lists of retrieved tweets (perhaps in the form of \texttt{(tweet\_text, location)} tuples) into XML templates which can be easily parsed by the frontend VXML app.

\subsubsection{GSL Grammars}

The system's grammars are written in GSL format, despite initial projections of using GrXML. The reason for using GSL was more out of prior familiarity than anything else. Having used it before, I was able to improve my knowledge of it more quickly than it would have taken for me to achieve a similar level of familiarity with GrXML.\\

The system uses two custom grammars, \texttt{activity.gsl} and \texttt{status.gsl}. Both of these grammars are fairly limited in the number of strings they generate. The Activity grammar is used to recognize utterances from the user which serve to switch between the various activities offered by the application --- posting tweets, listening to tweets, and the unimplemented activity of listening to info about Twitter friends. It recognizes utterances such as \textit{I would like to listen to some tweets}, \textit{I want to make a tweet}, \textit{Please let me check out a few of my friends}, \textit{Let's tweet something}, and so on. Though it is limited, the Activity grammar does offer recognition of a number of expressions of desire or demand to do something, with optional pleasantries. It also encodes a number of ways of referring to the activities of \textit{tweeting}, \textit{listening to tweets}, and \textit{hearing about friends}, including the optional use of varied quantifiers.\\

The system's Status grammar, used for recognizing utterances intended for transcription into status updates (posting tweets), is perhaps more limited than the Activity grammar, despite its use for an application which generally requires an open grammar. The Status grammar can recognize two basic types of utterance: An expression of how the user's day is going, and the expression of a question addressed to others, asking those others to join the user in a future activity. For example, the Status grammar will recognize \textit{i am having (such)? a (good $|$ great $|$ bad $|$ boring $|$ happy $|$ sad) day (today)?}, or \textit{(who wants $|$ does anybody want) to go (with me)? to the (bar $|$ concert $|$ movies $|$ park $|$ library) (later)? (tonight)?}. For the purposes of demonstrating the system's capabilities, this grammar provides sufficient variety in what it will allow the system to recognize and consequently post to the system's Twitter account.

\subsubsection{VoiceXML and Voxeo}

The frontend of the system, as mentioned, is a VoiceXML application running on Voxeo's servers. In order to communicate with the system's backend, the VXML application makes use of the VXML \texttt{$<$data$>$} element. This element allows HTTP requests to be made without submitting forms or navigating away from the main VXML document/application. This simplifies the implementation of the system frontend greatly.

\subsubsection{JavaScript and ECMAScript}

The facility with which one can manipulate DOM trees using JavaScript was a welcome advantage in implementing the system. Using JavaScript made it easy to accept responses from the backend in XML, parse out the desired data, and then iterate over it in the VXML app in order to present it to the user. In some instances, Voxeo's implementation of VoiceXML requires the use of ECMAScript rather than JavaScript. This posed a slight difficulty to debugging at times, but still retained much of the same efficiency as JavaScript.

\section{Challenges}

\subsection{Open-Ended Recognition Grammars}

One challenge which limits the current functionality of the system is the difficulty of constructing open-ended, open-domain recognition grammars for creating tweets to send to Twitter. Clearly, one might wish to say any conceivable English utterance (which maps to 140 characters or less) in order to have it posted as a Twitter status update. However, this is not a task to take on with hand-coded grammars. One could go about achieving such a goal by training a statistical language model. But the model would at least require a sizable training corpus, and it would still probably leave something to be desired in terms of recognition accuracy for numerous domains.\\

I did not attempt to train a statistical language model to use in my system, because I feared it would be a prohibitive endeavor in terms of time. As a result, I had to strike a balance between spending time hand-coding grammar rules and settling for a limited set of utterances that my system could recognize and post to Twitter. The resulting grammar is acceptable for the purposes of demonstrating my system's capabilities in a controlled setting, but a real-world system would need a more robust grammar to be useful.

\subsection{Authentication and OAuth}

After learning about Twitter's use of the OAuth authentication protocol, and what authentication steps OAuth requires, it became clear to me that I would not be able to allow different users to authenticate different Twitter user accounts with my system. This is due to the OAuth protocol's requirement that a user enter his/her username and password directly in a browser, into an HTML form served by the domain whose services one is attempting to authenticate into. I was unable to devise a solution to this problem, which means that my system cannot dynamically authenticate new users. As such, it is currently limited to authenticating a single test account for any and all system sessions.

\subsection{Username Grammars}

Even if I had been successful in figuring out how to dynamically authenticate users on a speech-based system using OAuth, I would have run into the problem of creating grammars that could reliably generate and recognize Twitter usernames. Twitter usernames, as well as numerous other kinds of username widely used on the internet, are often unpronounceable due to containing particular character patterns, such as the inclusion of digit characters throughout strings of alphabetical characters. This makes it quite difficult to write good grammars for recognizing and pronouncing usernames. One possible solution would be to concern oneself exclusively with usernames which happen to be words of English (e.g., the system's test account username, which is \textit{metatheoretical}). But such an approach unrealistically limits the range of recognizable usernames in real-world use.

\subsection{Sending and Receiving Data}

While building my system, I was under the impression that I would be able to freely use the JavaScript function \texttt{XMLHttpRequest} to create HTTP request objects for sending data back and forth between the system's frontend and backend. However, Voxeo's JavaScript implementation does not implement \texttt{XMLHttpRequest}. This forced me to search for an alternative. I discovered that the VXML \texttt{$<$data$>$} element would meet my requirements nicely, and instead used that, which proved simple and effective. In fact, using the \texttt{$<$data$>$} element required me to send tweets from the system backend to the frontend in XML format, which turned out to be a very convenient way to receive tweets on the frontend.

\section{Conclusion and Future Work}

In general, I consider the system to be a success. The system's dialogue management may be simple, but it is efficient and effective. The grammar for recognizing different desired activities is hand-coded, but surprisingly broad. The system does not have the intended feature of allowing users to hear information about their friends, nor does it have dynamic user authentication. However, the things the system does, it does well --- it provides a convenient, mostly hands- and eyes-free interface to a popular social networking platform for the creation and consumption of content.\\

In future work, I would like to implement the following additions to the system:

\begin{itemize}
\item dynamic user authentication, including dynamic username grammars
\item a more robust recognition grammar for creating tweets
\item the ability to attach metadata (e.g., location) to tweets posted from the system
\item the ability to hear information (name, location, last tweet, etc.) about friends
\item a mixed-initiative grammar for determining what the user would like to do with the system
\item switching between different user timelines for listening to tweets
\item intra-timeline navigation for easier selection of specific tweets to hear within a single timeline
\end{itemize}

\section{Related and Motivating Work}

The conceptualization of this system did not directly rely on any external publications. The ideas which were applied to its conceptualization were derived entirely from the course lectures, as well as experience with prior homework assignments.

\end{document}
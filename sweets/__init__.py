# Stefan Behr
# __init__.py
# LING 575

import os
import utils
import tweet
from flask import Flask, request, make_response, jsonify, render_template

# middleware for fixing routes on WebFaction
# drawn from Flask snippet 65 written by user esaurito
# at http://flask.pocoo.org/snippets/65/
class WebFactionMiddleware(object):
    def __init__(self, app):
        self.app = app
    def __call__(self, environ, start_response):
        environ['SCRIPT_NAME'] = '/sweets'
        return self.app(environ, start_response)

# Flask app
app = Flask(__name__)
app.wsgi_app = WebFactionMiddleware(app.wsgi_app)

# file directories
script_dir = os.path.split(os.path.abspath(__file__))[0]
json_dir = os.path.join(script_dir, "json")

# json file paths
consumer_path = os.path.join(json_dir, "consumer.json") # path to OAuth consumer key/secret
access_path = os.path.join(json_dir, "access.json") # path to OAuth access token key/secret

# OAuth keys and secrets
CONSUMER_KEY, CONSUMER_SECRET = utils.load_key_secret(consumer_path)
ACCESS_KEY, ACCESS_SECRET = utils.load_key_secret(access_path)

# tweeter object
tweeter = tweet.Tweeter(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_KEY, ACCESS_SECRET)

@app.route("/")
def dev():
    return "Hello. The sweets app is still in development."

@app.route("/tweet", methods=["POST"])
def post_tweet():
    """
    Posts a tweet to Twitter
    """
    text = request.form["tweet"]
    tweeter.tweet(text)
    return make_response()

@app.route("/listen/<username>/<int:count>")
def fetch_tweets(username, count):
    tweets = tweeter.get_tweets(username, count)
    return render_template("tweets.xml", username=username, tweets=tweets)

if __name__ == "__main__":
    app.run()

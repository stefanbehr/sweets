// file: main.js
// author: Stefan Behr
// course: LING 575

// like Python's str.strip method
// credit to StackOverflow user David Andres
// http://stackoverflow.com/a/1418059
if (typeof(String.prototype.trim) === "undefined") {
    String.prototype.trim = function() {
        return String(this).replace(/^\s+|\s+$/g, "");
    };
}

// generates functions specific to a DOMTree 
// for extracting text from first element in DOMTree 
// by certain tag names
function TextExtractorGenerator(DOMTree) {
    function getTextFromFirstElementByTagName(tagName) {
        var tagElements = DOMTree.getElementsByTagName(tagName);
        var tagElement = tagElements.item(0);
        var elementText = tagElement.firstChild.data.trim();
        return elementText;
    };
    return getTextFromFirstElementByTagName;
};

// function for extracting username of XML response document
function getUsername(XMLDocument) {
    return TextExtractorGenerator(XMLDocument)("username");
};

// function for creating list of tweet, location pairs 
// from XML response document
function getTweetList(XMLDocument) {
    var tweetElements = XMLDocument.getElementsByTagName("tweet");
    var tweetList = [];
    var i, tweetText, locText;
    for (i = 0; i < tweetElements.length; i++) {
        // make tweet DOM element-specific extractor
        var tweetContentExtractor = TextExtractorGenerator(tweetElements.item(i));
        var tweetText = tweetContentExtractor("text");
        var locText = tweetContentExtractor("loc");
        tweetList.push([tweetText, locText]);
    }
    return tweetList;
}

// can't use XMLHttpRequest
// var send_tweet = function(text) {
//     // send argument to backend to be sent 
//     // to twitter API as tweet
//     // inspiration taken from:
//     // http://www.openjs.com/articles/ajax_xmlhttp_using_post.php
// 
//     // url encode tweet
//     text = escape(text);
// 
//     var request = new XMLHttpRequest();
//     var url = "http://stefanbehr.net/sweets/tweet";
//     var params = "text=" + text;
// 
//     // open connection
//     request.open("POST", url, true);
// 
//     // set header info
//     request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//     request.setRequestHeader("Content-length", params.length);
//     request.setRequestHeader("Connection", "close");
//     request.send(params);
//     return request;
// };
# Stefan Behr
# tweet.py
# LING 575

import twitter as t

class Tweeter:
    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret):
        # create OAuth object
        master_key = t.OAuth(token=access_token,
                             token_secret=access_token_secret,
                             consumer_key=consumer_key,
                             consumer_secret=consumer_secret)

        # api objects
        self.timeline_api = t.Twitter(auth=master_key)
        self.stream_api = t.TwitterStream(auth=master_key)

        # constants
        self.limit = 140

    def tweet(self, content):
        """
        Post a tweet containing content, truncated to
        self.limit chars
        """
        self.timeline_api.statuses.update(status=content[:140])

    def get_tweets(self, username, n):
        """
        Get first up to n most recent tweets 
        for user with username
        """
        tweets = self.timeline_api.statuses.user_timeline(screen_name=username, count=n)
        stripped_tweets = []
        for tweet in tweets:
            text = tweet["text"]
            loc = tweet["place"]
            if not loc:
                loc = "unknown location"
            else:
                loc = loc["full_name"]
            stripped_tweets.append((text, loc))
        return stripped_tweets
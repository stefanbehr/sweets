# Stefan Behr
# utils.py
# LING 575

import json

def load_key_secret(fp):
    """
    Loads json object from given file path
    and extracts 'key' and 'secret' values
    """
    with open(fp) as f:
        obj = json.load(f)
    key = obj.get("key", "")
    sec = obj.get("secret", "")
    return key, sec

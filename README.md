Sweets
======
A speech interface to Twitter.

Proposed Feature Set
--------------------
* Selecting first N tweets from current timeline for TTS (default and baseline is public timeline)
* User authentication for simple username/password pairs
* Switching between different user timelines, assuming simple usernames
* Generating and switching to timelines based on datetime
* Generating and switching to timelines based on location
* Posting tweets to your timeline (authentication required)
  * Initially limited to restricted set of possible utterances